'use strict';

const configDB = require('../config').database;
const mysql = require('mysql');

exports.connect = () => {
    const connection = mysql.createConnection({
        host: configDB.host,
        user: configDB.username,
        password: configDB.password
    });
    return connection;
}