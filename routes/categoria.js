'use strict';

// * PLUGINS
const express = require('express');
const router = express.Router();

// * CONTROLADORES
const controller = require('../controllers/categoria');

// * RUTAS
// Obtiene litado de categorias
router.get('/listado', controller.listado);

module.exports = router;