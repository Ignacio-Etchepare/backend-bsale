'use strict';

// * PLUGINS
const express = require('express');
const router = express.Router();

// * CONTROLADORES
const controller = require('../controllers/producto');

// * RUTAS
// Obtiene litado de productos
router.get('/listado', controller.listado);

module.exports = router;