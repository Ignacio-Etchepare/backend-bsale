# Test de Postulación para BSale
## **Ignacio Etchepare**

Proyecto backend de sistema de carro de compras de productos.

Se crea una aplicación con NodeJS con framework Express, la estructura de la api esta ordenada en varias carpetas.

## Requeridos

Se necesita instalar NodeJS, de preferencia versión 12.14.0

[Descarga](https://nodejs.org/download/release/v12.14.0/)


## Instalación

Debe estar en la carpeta del backend y ejecutar el siguiente comando:

```javascript
npm install
```

## Uso

Debe estar en la carpeta del backend y ejecutar el siguiente comando:

```javascript
npm start
```

# API
## Routes

```javascript
// Endpoint para utilizar el controlador de listado de productos 
/producto/listado
```


```javascript
// Endpoint para utilizar el controlador de listado de categorías 
/categoria/listado
```


## Functions

connection.js

Conexión a base de datos MySQL

## Controllers

- categoria.js

Controlador que contiene y utiliza funciones de categorias.

- producto.js

Controlador que contiene y utiliza funciones de productos.

## config.js

Configuracion y conexión para la utilización de base de datos MySQL.

## app.js

Aplicacion donde se crea con express la instancia de servidor utilizando rutas del proyecto.


# Deploy

El deploy se encuentra alojado en Heroku y se accede por medio del siguiente enlace:

**[https://bsale-frontend-ietchepare.herokuapp.com](https://bsale-frontend-ietchepare.herokuapp.com)**


Se utiliza plataforma GitLab para los repositorios de backend y de frontend del proyecto:

### Backend
[https://gitlab.com/Ignacio-Etchepare/backend-bsale](https://gitlab.com/Ignacio-Etchepare/backend-bsale)


### Frontend
[https://gitlab.com/Ignacio-Etchepare/bsale-frontend](https://gitlab.com/Ignacio-Etchepare/bsale-frontend)

