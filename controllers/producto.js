'use strict';

const connectionFunction = require('../functions/connection')

// Listado de productos
exports.listado = async (req, res) => {
    try {

        // Se conecta a BD MySQL
        const connection = connectionFunction.connect();

        const { nombre, categoriaId } = req.query;

        connection.connect();

        let where = '';
        if (nombre || categoriaId) {
            const filtros = [];
            if (nombre) {
                filtros.push(` prod.name LIKE '%${nombre}%' `)
            }
            if (categoriaId) {
                filtros.push(` cat.id = ${categoriaId} `)
            }
            where = `WHERE ${filtros.join('AND')}`
        }

        const query = `
            SELECT prod.*, cat.name AS category_name
            FROM bsale_test.product prod
            LEFT JOIN bsale_test.category cat ON prod.category = cat.id
            ${where};
        `

        const rows = await new Promise((resolve, reject) => {
            connection.query(query, (error, results) => {
                if (error) return reject(error);

                return resolve(results)
            })
        });

        connection.end();

        return res.status(200).json({ data: rows, total: rows.length });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}