'use strict';

const connectionFunction = require('../functions/connection')

// Listado de productos
exports.listado = async (req, res) => {
    try {

        // Se conecta a BD MySQL
        const connection = connectionFunction.connect();

        connection.connect();

        const query = `
            SELECT *
            FROM bsale_test.category;
        `

        const rows = await new Promise((resolve, reject) => {
            connection.query(query, (error, results) => {
                if (error) return reject(error);

                return resolve(results)
            })
        });

        connection.end();

        return res.status(200).json({ data: rows, total: rows.length });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}