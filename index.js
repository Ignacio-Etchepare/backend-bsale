'use strict';

// * PLUGINS
const http = require('http');

// * APP
const app = require('./app');

// * CONFIG
const port = process.env.PORT || 5000;

app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Servidor UP\n - PUERTO ${port} `))
